"use strict";
$(document).ready(function() {
    /*tinymce.init({
        selector: 'textarea',
        menubar: false,
        plugins: ['advlist autolink lists link image charmap print preview hr anchor pagebreak', 'searchreplace wordcount visualblocks visualchars code fullscreen', 'insertdatetime media nonbreaking save table contextmenu directionality', 'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'],
        toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image preview | forecolor backcolor | codesample',
        image_advtab: true
    });*/
    $(document).on('change', '#disc_avail', function() {
        var disc_avail = $(this).val();
        alert(disc_avail);
        if(disc_avail == 1) {
            $('#disc_type').prop('disabled', false);
            $('#discount').prop('disabled', false);
        } else {
            $('#disc_type').prop('disabled', 'disabled');
            $('#discount').prop('disabled', 'disabled');
        }
    });

    /*
    email: {
        required: true,
        email: true,
    },
    phone: {
        required: true,
        numbersonly: true
    },
    destination: "required",


    email: {
                required: "Please enter an email",
                email: "Please enter valid email",
            }



    */
    $.validator.addMethod( "lettersonly", function( value, element ) {
        return this.optional( element ) || /^[a-zA-Z\s]+$/.test( value );
    }, "Only alphabets and spaces are allowed" );

    $.validator.addMethod( "float_validation", function( value, element ) {
        return this.optional( element ) || /^\d{0,4}(\.\d{0,2})?$/.test( value );
    }, "Invalid data provided" );

    /* Product Category validation - Start*/
    $("#prod_cat").validate({
        rules: {
            name: {
                required: true,
                lettersonly: true,
                maxlength: 100
            }
        },
        messages: {
            name: {
                required: "Please enter category name"
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
    /* Validation end*/

    /* Product validation - Start*/
    $("#prod_form").validate({
        rules: {
            name: {
                required: true,
                maxlength: 250
            },
            cat_id: {
                required: true
            },
            description: {
                required: true
            },
            stock: {
                required: true,
                digits:true
            },
            unit_price: {
                required: true,
                float_validation: true
            },
            unit_weight: {
                required: true,
                digits:true
            },
            discount: {
                float_validation: true
            }
        },
        messages: {
            name: {
                required: "Please product name"
            },
            cat_id: {
                required: "Please select category for product"
            },
            description: {
                required: "Product description cannot be empty"
            },
            stock: {
                required: "Product stock cannot be empty",
                digits:"Only numbers allowed"
            },
            unit_price: {
                required: "Please enter product price"
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
    /* Validation end*/

    /* Slider validation - Start*/
    $("#slider_form").validate({
        rules: {
            name: {
                required: true,
                lettersonly: true,
                maxlength: 100
            },
            rank: {
                required: true,
                digits: true
            }
        },
        messages: {
            name: {
                required: "Please enter slider name"
            },
            rank: {
                required: "Please enter slider rank",
                digits: "Only numeric values allowed"
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
    /* Validation end*/

    $(".card-header-right .close-card").on('click', function() {
        var $this = $(this);
        $this.parents('.card').animate({
            'opacity': '0',
            '-webkit-transform': 'scale3d(.3, .3, .3)',
            'transform': 'scale3d(.3, .3, .3)'
        });
        setTimeout(function() {
            $this.parents('.card').remove();
        }, 800);
    });

    $(".card-header-right .reload-card").on('click', function() {
        var $this = $(this);
        $this.parents('.card').addClass("card-load");
        $this.parents('.card').append('<div class="card-loader"><i class="fa fa-spinner rotate-refresh"></div>');
        setTimeout(function() {
            $this.parents('.card').children(".card-loader").remove();
            $this.parents('.card').removeClass("card-load");
        }, 3000);
    });
    $(".card-header-right .card-option .fa-chevron-left").on('click', function() {
        var $this = $(this);
        if ($this.hasClass('fa-chevron-right')) {
            $this.parents('.card-option').animate({
                'width': '35px',
            });
        } else {
            $this.parents('.card-option').animate({
                'width': '190px',
            });
        }
        $(this).toggleClass("fa-chevron-right").fadeIn('slow');
    });
    $(".card-header-right .minimize-card").on('click', function() {
        var $this = $(this);
        var port = $($this.parents('.card'));
        var card = $(port).children('.card-block').slideToggle();
        $(this).toggleClass("fa-minus").fadeIn('slow');
        $(this).toggleClass("fa-plus").fadeIn('slow');
    });
    $(".card-header-right .full-card").on('click', function() {
        var $this = $(this);
        var port = $($this.parents('.card'));
        port.toggleClass("full-card");
        $(this).toggleClass("fa-window-restore");
    });
    $(".card-header-right .icofont-spinner-alt-5").on('mouseenter mouseleave', function() {
        $(this).toggleClass("rotate-refresh").fadeIn('slow');
    });
    $("#more-details").on('click', function() {
        $(".more-details").slideToggle(500);
    });
    $(".mobile-options").on('click', function() {
        $(".navbar-container .nav-right").slideToggle('slow');
    });
    $(".search-btn").on('click', function() {
        $(".main-search").addClass('open');
        $('.main-search .form-control').animate({
            'width': '200px',
        });
    });
    $(".search-close").on('click', function() {
        $('.main-search .form-control').animate({
            'width': '0',
        });
        setTimeout(function() {
            $(".main-search").removeClass('open');
        }, 300);
    });
    $(document).ready(function() {
        $(".header-notification").click(function() {
            $(this).find(".show-notification").slideToggle(500);
            $(this).toggleClass('active');
        });
    });
    $(document).on("click", function(event) {
        var $trigger = $(".header-notification");
        if ($trigger !== event.target && !$trigger.has(event.target).length) {
            $(".show-notification").slideUp(300);
            $(".header-notification").removeClass('active');
        }
    });
    $.mCustomScrollbar.defaults.axis = "yx";
    $("#styleSelector .style-cont").slimScroll({
        setTop: "1px",
        height: "calc(100vh - 495px)",
    });
    $(".main-menu").mCustomScrollbar({
        setTop: "1px",
        setHeight: "calc(100% - 56px)",
    });
    var a = $(window).height() - 80;
});
$(document).ready(function() {
    $(".theme-loader").animate({
        opacity: "0"
    }, 1000);
    setTimeout(function() {
        $(".theme-loader").remove();
    }, 800);
});

function toggleFullScreen() {
    var a = $(window).height() - 10;
    if (!document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement) {
        if (document.documentElement.requestFullscreen) {
            document.documentElement.requestFullscreen();
        } else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
        } else if (document.documentElement.webkitRequestFullscreen) {
            document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
        }
    } else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        }
    }
}
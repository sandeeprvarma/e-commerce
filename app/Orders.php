<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
	protected $fillable = ['cust_id','payment_id','order_date','delivery_date','ship_date',	'sales_tax','total','fulfilled','deleted','paid','payment_date'];

	/*public function order_details()
    {
        return $this->hasMany(OrderDetails::class, 'order_id');
    }*/
}

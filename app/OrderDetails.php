<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{
    protected $table = 'orderdetails';
    protected $fillable = ['order_id','product_id','price','quantity','discount','total','bill_date','ship_date','fulfilled','salesTax'];

    /*public function product() {
    	return $this->hasOne('App\Product');
    }*/

    public function product()
    {
        return $this->hasMany('App\Product','product_id');
    }


    public function order()
    {
        return $this->belongsTo('App\Order','order_id');
    }
}

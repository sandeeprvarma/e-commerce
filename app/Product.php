<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $attributes = [
		'unit_size' => 0.00,
		'msrp' => 0.00,
		'reorder_level' => 0,
		'note' => '',
		'ranking' => 0
	];
	
    protected $fillable = ['name','description','cat_id','is_active','unit_size','unit_price','unit_weight','weight_class','units_in_stocks','msrp','disc_available','disc_type','discount','units_on_order','reorder_level','thumb','ranking','note'];

}

<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function jsonResponse($status, $message, $data, $http_code = 200, $error_code = '')
	{
		// $data = json_encode($data,JSON_FORCE_OBJECT);
		if(empty($error_code)) {
			return response()->json(['status' => $status, 'message' => $message,'data' => $data], $http_code);
		}else {
			return response()->json(['status' => $status, 'message' => $message,'data' => $data, 'error_code' => $error_code], $http_code);
		}
	}

	/*
	1001: Missing paarmeters for registration  
	1002
	1003
	1004
	1005
	1006
	1007
	1008
	1009
	1010
	1011
	1012
	1013
	1014

	1201: mobile not verified
	1202
	1203
	1204
	1205
	1206
	1207
	1208
	1209
	1210
	1211
	1212

	13XX: not found
	1301
	1302
	1303
	1304
	1305
	1306
	1307
	1308
	1309
	1310
	1311
	1312
	1313


	14XX: Exception
	1401
	1402
	1403
	1404
	1405
	1406
	1407
	1408
	1409
	1410
	1411
	1412
	1413
	1414
	1415
	1416
	1417
	1418
	1419
	1420
	1421
	1422
	1423
	1424
	1425
	1426
	1427
	
	*/

}

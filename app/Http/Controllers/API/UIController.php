<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Service\UICustomizationService;

class UIController extends Controller
{
    public function index()
    {
        try{
            $slider_data = UICustomizationService::getSlidersData();
            if(empty($slider_data)) {
                return $this->jsonResponse(false, 'Slider data has not been added yet. Please contact admin.', [], 200,1313);
            }else {
                return $this->jsonResponse(true, 'Success', $slider_data, 200);
            }
        }catch(\Exception $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1427);
        }
    }
}

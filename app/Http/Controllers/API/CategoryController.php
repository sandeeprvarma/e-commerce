<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Service\CategoryService;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $category = CategoryService::getAllCategory();
            if(empty($category)) {
                return $this->jsonResponse(false, 'No product category found', [], 200,1304);
            }else {
                return $this->jsonResponse(true, 'Category found', $category, 200);
            }
        }catch(\Exception $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1407);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $category = CategoryService::getCategoryId($id);
            if(empty($category)) {
                return $this->jsonResponse(false, 'No product category found', [], 200,1305);
            }else {
                return $this->jsonResponse(true, 'Category found', $category, 200);
            }
        }catch(\Exception $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1408);
        }
    }

    /**
     * Display the specified resource using pref id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getCategoryByPref($id)
    {
        try{
            $category = CategoryService::getCategoryByPref($id);
            if(empty($category)) {
                return $this->jsonResponse(false, 'No product category found', [], 200,1306);
            }else {
                return $this->jsonResponse(true, 'Category found', $category, 200);
            }
        }catch(\Exception $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1409);
        }
    }    
}

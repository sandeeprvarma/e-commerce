<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Service\AddressService;
use Illuminate\Support\Facades\Validator;
use JWTAuth;

class CustomerAddressController extends Controller
{

    public function __construct()
    {
        try {
            $this->user = JWTAuth::parseToken()->authenticate();
            if(!isset($this->user->id)) {
                return $this->jsonResponse(false, 'Token invalid', [], 401, 1207);
            }
        }catch(\Exception $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1426);
        }
    }

    /**
     * Display addresses of a customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAddresses($address_type = '')
    {
        try{
            $address = AddressService::getAllAddresses($this->user->id,$address_type);
            if(empty($address)) {
                return $this->jsonResponse(false, 'No address found', [], 200,1307);
            }else {
                return $this->jsonResponse(true, 'Address found', $address, 200);
            }
        }catch(\Exception $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1410);
        }
    }

    public function addAddress(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'address_type' => 'required|integer|max:10',
                'house_number' => 'required|string|max:15',
                'block_name' => 'nullable|string|max:150',
                'building_name' => 'nullable|string',
                'street' => 'required|string',
                'landmark' => 'nullable|string',
                'zipcode' => 'required|integer|min:6',
                'locality' => 'nullable|string',
                'city' => 'nullable|string',
                'state' => 'nullable|string',
                'country' => 'nullable|string',
                'mobile' => 'required|string',
                'is_active' => 'nullable|integer'
            ]);

            if($validator->fails()){
                return $this->jsonResponse(false, 'Invalid parameters', $validator->errors(), 400,1010);
            }
            $input = $request->all();
            $input['customer_id'] = $this->user->id;
            AddressService::addAddress($input);
        }catch(\BadMethodCallException $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1411);
        }catch(\Exceptions $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1412);
        }
        return $this->jsonResponse(true, 'Address added successfully.', [], 201);
    }

    public function editAddress($id, Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'address_type' => 'required|integer|max:10',
                'house_number' => 'required|string|max:15',
                'block_name' => 'nullable|string|max:150',
                'building_name' => 'nullable|string',
                'street' => 'required|string',
                'landmark' => 'nullable|string',
                'zipcode' => 'required|integer|min:6',
                'locality' => 'nullable|string',
                'city' => 'required|string',
                'state' => 'required|string',
                'country' => 'required|string',
                'mobile' => 'required|string',
                'is_active' => 'required|integer'
            ]);

            if($validator->fails()){
                return $this->jsonResponse(false, 'Invalid parameters', $validator->errors(), 400,1011);
            }
            $input = $request->all();
            $input['customer_id'] = $this->user->id;
            AddressService::editAddress($id, $input);
        }catch(\BadMethodCallException $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1413);
        }catch(\Exceptions $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1414);
        }
        return $this->jsonResponse(true, 'Address updated successfully.', [], 200);
    }

    public function deleteAddress($id)
    {
        try{
            AddressService::deleteAddress($id);
            return $this->jsonResponse(true, 'Address deleted successfully.', [], 200);
        }catch(\Exception $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1415);
        }
    }
}

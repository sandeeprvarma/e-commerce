<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Service\CartService;
use App\Http\Service\CustomerService;
use Illuminate\Support\Facades\Validator;
use JWTAuth;

class CartController extends Controller
{
    public function __construct()
    {
        try {
            $this->user = JWTAuth::parseToken()->authenticate();
            if(!isset($this->user->id)) {
                return $this->jsonResponse(false, 'Token invalid', [], 401, 1207);
            }
        }catch(\Exception $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1426);
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $cart = CartService::getCartByCustomerId($this->user->id);
            if(empty($cart)) {
                return $this->jsonResponse(false, 'Cart is empty', [], 200,1309);
            }else {
                return $this->jsonResponse(true, 'Products found in cart', $cart, 200);
            }
        }catch(\Exception $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1416);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'product_id' => 'required|integer',
                'quantity' => 'required',
                'price' => 'nullable|string',
                'details' => 'nullable|string'
            ]);

            if($validator->fails()){
                return $this->jsonResponse(false, 'Invalid parameters', $validator->errors(), 400,1012);
            }
            $cart = CartService::addToCart($this->user->id,$request->all());
            if(!$cart) {
                return $this->jsonResponse(false, 'No product found with this id', [], 200,1310);
            }
        }catch(\BadMethodCallException $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1417);
        }catch(\Exceptions $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1418);
        }
        return $this->jsonResponse(true, 'Added to cart successfully.', [], 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    // {
    //     try{
    //         $validator = Validator::make($request->all(), [
    //             'product_id' => 'required|integer',
    //             'quantity' => 'required',
    //             'price' => 'nullable|string',
    //             'details' => 'nullable|string'
    //         ]);

    //         if($validator->fails()){
    //             return $this->jsonError(400,$validator->errors());
    //         }
    //         CartService::updateCart($id,$request->all());
    //     }catch(\BadMethodCallException $e) {
    //         return $this->jsonError(500,$e->getMessage());
    //     }catch(\Exceptions $e) {
    //         return $this->jsonError($e->getStatusCode(),$e->getMessage());
    //     }
    //     return $this->jsonResponse(200,"Cart updated successfully.");
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'product_id' => 'required|integer|max:10',
            ]);

            if($validator->fails()){
                return $this->jsonResponse(false, 'Invalid parameters', $validator->errors(), 400,1013);
            }
            CartService::removeFromCart($this->user->id,$request->product_id);
        }catch(\BadMethodCallException $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1419);
        }catch(\Exceptions $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1420);
        }
        return $this->jsonResponse(true, 'Cart updated successfully.', [], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function empty()
    {
        try{
            CartService::emptyCart($this->user->id);
        }catch(\BadMethodCallException $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1421);
        }catch(\Exceptions $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1422);
        }
        return $this->jsonResponse(true, 'All items are deleted from cart', [], 200);
    }

    public function getCartSummary()
    {
        try{
            $cart = CartService::getCartSummary($this->user->id);
            if($cart) {
                return $this->jsonResponse(false, 'Cart is empty', [], 200,1311);
            }else {
                return $this->jsonResponse(true, 'Products found in the cart', $cart, 200);
            }
        }catch(\Exception $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1423);
        }
    }
}

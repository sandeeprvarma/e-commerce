<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Service\OrdersService;
use App\Http\Service\CartService;
use JWTAuth;

class OrderController extends Controller
{
    public function __construct()
    {
        try {
            $this->user = JWTAuth::parseToken()->authenticate();
            if(!isset($this->user->id)) {
                return $this->jsonResponse(false, 'Token invalid', [], 401, 1207);
            }
        }catch(\Exception $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1426);
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $orders = OrdersService::getAllorders($this->user->id);
            if(empty($orders)) {
                return $this->jsonResponse(false, 'No new orders', [], 200,1312);
            }else {
                return $this->jsonResponse(true, 'Orders data', $orders, 200);
            }
        }catch(\Exception $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1424);
        }    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $input = $request->all();
            $validator = Validator::make($input, [
                'address_id' => 'required|integer',
            ]);

            if($validator->fails()){
                return $this->jsonResponse(false, 'Invalid parameters', $validator->errors(), 400,1014);
            }
            $cart_data = OrdersService::createOrder($this->user->id,$input);

            if($cart_data) {
                return $this->jsonResponse(true, 'Order placed successfully', [], 201);
            }else {
                return $this->jsonResponse(false, 'Failed to place order', [], 400,1209);
            }
        }catch(\Exception $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1425);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

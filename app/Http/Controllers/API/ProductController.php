<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Service\ProductService;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $products = ProductService::getAllProducts();
            if(empty($products)) {
                return $this->jsonResponse(false, 'No product found', [], 200,1301);
            }else {
                return $this->jsonResponse(true, 'Products found', $products, 200);
            }
        }catch(\Exception $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1404);
        }
    }

    /**
     * Get product using category id
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getProductsByCategory($id)
    {
        try{
            $products = ProductService::getProductByCategory($id);
            if(empty($products)) {
                return $this->jsonResponse(false, 'No product found', [], 200,1302);
            }else {
                return $this->jsonResponse(true, 'Products found', $products, 200);
            }
        }catch(\Exception $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1405);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $products = ProductService::getProductById($id);
            if(empty($products)) {
                return $this->jsonResponse(false, 'No product found', [], 200,1303);
            }else {
                return $this->jsonResponse(true, 'Products found', $products, 200);
            }
        }catch(\Exception $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1406);
        }
    }

    public function search(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'search' => 'required',
            ]);

            if($validator->fails()){
                return $this->jsonResponse(false, 'Invalid parameters', $validator->errors(), 400,1014);
            }
            $products = ProductService::searchProducts($request->search);
            if(empty($products)) {
                return $this->jsonResponse(false, 'No product found', [], 200,1303);
            }else {
                return $this->jsonResponse(true, 'Products found', $products, 200);
            }
        }catch(\Exception $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1406);
        }
    }
}

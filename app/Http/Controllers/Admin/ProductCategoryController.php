<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\ProductCategory;

class ProductCategoryController extends Controller
{
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = ProductCategory::all();
        return view('admin.product_category.product_category')->with('categories',$categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parents = ProductCategory::all();
        return view('admin.product_category.add_product_category')->with('parents',$parents);;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:100',
                'parent' => 'required',
                'description' => 'required',
                'thumb' => 'required|mimes:jpeg,jpg,png,gif,svg|max:2048'
            ]);

            if ($validator->fails()) {
                return back()
                        ->withErrors($validator)
                        ->withInput();
            }

            $thumb = $this->uploadImage('thumb', $request);

            $prod_cat = new ProductCategory;
            $prod_cat->name = $request->name;
            $prod_cat->parent_id = $request->parent;
            $prod_cat->description = $request->description;
            $prod_cat->thumb = 'images/uploads/'.$thumb;
            $prod_cat->is_active = $request->status;
            $prod_cat->pref_type = $request->pref;            
            $prod_cat->save();

            $message = 'Product category added successfully.';
        }catch(\Exception $e) {
            return $e->getMessage();
        }

        return back()->with('success',$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = ProductCategory::find($id);
        //$parents = ProductCategory::all();\
        $parents = ProductCategory::where('id', '!=',$id)->get();
        return view('admin.product_category.edit_product_category')->with(['product' => $product, 'parents' => $parents]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100',
            'description' => 'required'
        ]);
        if ($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }

        try {
            $prod_cat = ProductCategory::find($id);
            $thumb = $this->uploadImage('thumb', $request);

            $prod_cat->name = $request->name;
            $prod_cat->parent_id = $request->parent;
            $prod_cat->description = $request->description;
            $prod_cat->thumb = !empty($thumb)?'images/uploads/'.$thumb:$prod_cat->thumb;
            $prod_cat->is_active = $request->status;
            $prod_cat->pref_type = $request->pref;
            $prod_cat->save();
            $message = 'Product category updated Successfully.';
        } catch(\Exception $e) {
            return $e->getMessage();
        }
        return redirect()->back()->with('success', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProductCategory::destroy($id);
        return redirect()->back()->with('success', 'Product category deleted successfully.');
    }

    /**
	* Custom function to upload image on the server
	* @param string $image_name, Request
	* @return Image name/false
    */

    public function uploadImage($image_name,Request $request)
    {
        if($request->hasFile($image_name)) {
            $image = $request->file($image_name);
            $name = md5(time().$image_name).'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images/uploads');
            $image->move($destinationPath,$name);
            return $name;
        } else {
            return false;
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Orders;
use App\OrderDetails;
use App\Product;
use DB;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Orders::join('customers', function($join) {
                $join->on('customers.id', '=', 'orders.cust_id');
            })
            ->get([
                'orders.id',
                'orders.fulfilled',
                'orders.delivery_date',
                'orders.total',
                'customers.first_name',
                'customers.last_name'
            ]);
        
        return view('admin.orders.view_orders')->with('orders',$orders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*SELECT pd.thumb, pd.name, od.quantity, od.price, od.total FROM `ftp_orderdetails` as od join `ftp_products` as pd on od.product_id = pd.id where od.order_id = 1
*/
        //$order_details = OrderDetails::where('order_id' , $id)->get();
        /*$orders = Orders::find($id);*/
        /*$orders = Orders::join('customers', function($join) {
                $join->on('customers.id', '=', 'orders.cust_id');
            })
            ->where('orders.id', '=', $id)
            ->get([
                'orders.id',
                'orders.fulfilled',
                'orders.delivery_date',
                'customers.first_name',
                'customers.last_name'
            ]);*/
        $orders = DB::table('orders')
               ->join('customers', 'customers.id', '=', 'orders.cust_id')
               ->join('address', 'address.id', '=', 'orders.address_id')
               ->select('orders.*','customers.first_name','customers.last_name','customers.email','customers.std_code','customers.mobile','address.house_number','address.building_name')
               ->where('orders.id', '=', $id)
               ->get();

        $order_details = Product::join('orderdetails', function($join) {
                $join->on('products.id', '=', 'orderdetails.product_id');
            })
            ->where('orderdetails.order_id', '=', $id)
            ->get([
                'products.thumb',
                'products.name',
                'orderdetails.quantity',
                'orderdetails.price',
                'orderdetails.total'
            ]);


        return view('admin.orders.view_order_detail')->with(['order_details' => $order_details, 'orders' => $orders]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Orders::destroy($id);
        return redirect()->back()->with('success', 'Order deleted successfully.');
    }
}

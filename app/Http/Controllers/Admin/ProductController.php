<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Product;
use App\ProductImages;
use App\ProductCategory;

class ProductController extends Controller
{
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('admin.product.product')->with('products',$products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = ProductCategory::all();
        return view('admin.product.add_product')->with('categories',$categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:100',
                'description' => 'required',                
                'unit_price' => 'required',
                'stock' => 'required',
                'thumb' => 'required|mimes:jpeg,jpg,png,gif,svg|max:2048',
                'unit_weight' => 'required',
                'weight_class' => 'required'
            ]);

            if ($validator->fails()) {
                return back()
                        ->withErrors($validator)
                        ->withInput();
            }

            $thumb = $this->uploadImage('thumb', $request);

            $prod = new Product;
            $prod->name = $request->name;
            $prod->cat_id = $request->cat_id;
            $prod->description = $request->description;
            $prod->unit_price = $request->unit_price;
            $prod->units_in_stocks = $request->stock;
            $prod->thumb = 'images/uploads/'.$thumb;
            $prod->unit_weight = $request->unit_weight;
            $prod->unit_weight_class = $request->weight_class;
            $prod->is_active = $request->is_active;
            $prod->units_on_order = 0;
            $prod->disc_type = $request->disc_type??0;
            $prod->discount = $request->discount??0.00;
            $prod->disc_available = $request->disc_avail;
            $prod->save();

            $product_id = $prod->id;
            $images = $request->file('add_thumb');
            if ($request->hasFile('add_thumb')) {
                foreach ($images as $key => $item) {
                    $prod_images = new ProductImages;
                    $add_thumb = $this->uploadMultipleImage('add_thumb'.$key, $item);
                    $prod_images->product_id = $product_id;
                    $prod_images->image = 'images/uploads/'.$add_thumb;
                    $prod_images->save();
                }
            }

            $message = 'Product added successfully.';
        }catch(\Exception $e) {
            return $e->getMessage();
        }

        return back()->with('success',$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $categories = ProductCategory::all();
        return view('admin.product.edit_product')->with(['product' => $product, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
           'name' => 'required|max:100',
            'description' => 'required',                
            'unit_price' => 'required',
            'stock' => 'required',
            'unit_weight' => 'required',
            'weight_class' => 'required'
        ]);
        if ($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }

        try {
            $thumb = $this->uploadImage('thumb', $request);

            $prod = Product::find($id);
            $prod->name = $request->name;
            $prod->cat_id = $request->cat_id;
            $prod->description = $request->description;
            $prod->unit_price = $request->unit_price;
            $prod->units_in_stocks = $request->stock;
            $prod->thumb = !empty($thumb)?'images/uploads/'.$thumb:$prod->thumb;
            $prod->unit_weight = $request->unit_weight;
            $prod->unit_weight_class = $request->weight_class;
            $prod->is_active = $request->is_active;
            $prod->units_on_order = $prod->units_on_order;
            $prod->disc_available = $request->disc_avail;
            $prod->disc_type = $request->disc_type??$prod->disc_type;
            $prod->discount = $request->discount??$prod->discount;
            $prod->save();
            $message = 'Product updated Successfully.';
        } catch(\Exception $e) {
            return $e->getMessage();
        }
        return redirect()->back()->with('success', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::destroy($id);
        return redirect()->back()->with('success', 'Product deleted successfully.');
    }

    /**
	* Custom function to upload image on the server
	* @param string $image_name, Request
	* @return Image name/false
    */

    public function uploadImage($image_name,Request $request)
    {
        if($request->hasFile($image_name)) {
            $image = $request->file($image_name);
            $name = md5(time().$image_name).'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images/uploads');
            $image->move($destinationPath,$name);
            return $name;
        } else {
            return false;
        }
    }

    public function uploadMultipleImage($image_name, $image)
    {
        $name = md5(time().$image_name).'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/images/uploads');
        $image->move($destinationPath,$name);
        return $name;
    }
}

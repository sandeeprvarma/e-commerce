<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Service\OrdersService;
use App\Http\Service\CustomerService;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        /*$this->middleware('auth');*/
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $revenue = OrdersService::getRevenue();
        $count = CustomerService::getcustomerCount();
        $daily_orders = OrdersService::getDailyOrders();
        $data = array('revenue'=>$revenue,'customer_count'=>$count,'daily_orders'=>$daily_orders);
        return view('admin.dashboard')->with('data',$data);
    }
}

<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Service\OTP;
use App\Http\Service\CommonFunction;

class AuthController extends Controller
{
    public function isUserVerified($mobile) {
        try {
            $is_verified = OTP::isVerified($mobile);
            if(!$is_verified) {
                return $this->jsonResponse(false, 'Mobile number not verified', [], 400,1004);
            } else {
                return $this->jsonResponse(true, 'Verified user', [], 200);
            }
        } catch(\Exceptions $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1401);
        }
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('mobile', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return $this->jsonResponse(false, 'Invalid credentials', [], 400,1005);
            }
        } catch (JWTException $e) {
            return $this->jsonResponse(false, 'Could not create token', [], 500,1202);
        }
        return $this->jsonResponse(true, 'Could not create token', compact('token'), 200);
    }

    public function register(Request $request)
    {
        try{
            $mobile = CommonFunction::handleCountryCode($request->get('mobile'));
            $request->merge(['mobile' => $mobile]);
            $validator = Validator::make($request->all(), [
                'first_name' => 'required|string|max:150',
                'last_name' => 'required|string|max:150',
                'mobile' => 'required|string|min:10|max:15|unique:customers',
                'email' => 'required|string|email|max:150|unique:customers',
                'password' => 'required|string|min:6|confirmed',
                'preference' => 'required'
            ]);

            if($validator->fails()){
                return $this->jsonResponse(false, 'Invalid parameters', $validator->errors(), 400,1001);
            }
            $is_verified = OTP::isVerified($request->get('mobile'));

            if(!$is_verified) {
                return $this->jsonResponse(false, 'Mobile number not verified', [], 400,1002);
            }

            $user = Customer::create([
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'mobile' => $request->get('mobile'),
                'username' => $request->get('username')??'',
                'std_code' => $request->get('std_code')??'+91',
                'is_active' => $request->get('is_active')??1,
                'email' => $request->get('email'),
                'preference' => $request->get('preference'),
                'password' => Hash::make($request->get('password')),
            ]);

            $token = JWTAuth::fromUser($user);
        }catch(\Exceptions $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1003);
        }
        return $this->jsonResponse(true, 'Successfully regitered a new user', compact('user','toke'), 201);
    }

    public function getAuthenticatedUser()
    {
        try {
            
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return $this->jsonResponse(false, 'User not found', [], 404, 1203);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return $this->jsonResponse(false, 'Token expired', [], 401, 1204);
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return $this->jsonResponse(false, 'Token invalid', [], 401, 1205);
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return $this->jsonResponse(false, 'Token absent', [], 401, 1206);
        }
        return $this->jsonResponse(true, 'User authentication successful', compact('user'), 200);
    }

    public function sendOTP(Request $request)
    {
        try {
            $mobile_with_code = $request->get('mobile');
            $mobile = CommonFunction::handleCountryCode($request->get('mobile'));
            $request->merge(['mobile' => $mobile]);

            $validator = Validator::make($request->all(), [
                'mobile' => 'required|string|min:10|max:15|unique:customers',
            ]);

            if($validator->fails()){
               return $this->jsonResponse(false, 'Invalid parameters', $validator->errors(), 400,1006);
            }
            OTP::sendOTP($mobile_with_code);
            return $this->jsonResponse(true, 'OTP sent successfully', [], 200);
        } catch (\Exception $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1402);
        }
    }

    protected function verify(Request $request)
    {
        try{
            $validator = Validator::make($request->all(),[
                'otp' => 'required|numeric',
                'mobile' => 'required|string',
            ]);
            if($validator->fails()){
                return $this->jsonResponse(false, 'Invalid parameters', $validator->errors(), 400,1007);
            }
            $verified = OTP::verifyOTP($request->get('mobile'),$request->get('otp'));
            if($verified) {
                return $this->jsonResponse(true, 'Mobile number verified successfully', [], 200);
            }else {
                return $this->jsonResponse(false, 'Mobile number verification failed.', [], 400,1008);
            }
        }catch(\Exceptions $e) {
            return $this->jsonResponse(false, $e->getMessage(), [], 500,1403);
        }
        return $this->jsonResponse(false, 'Invalid verification code entered!', [], 400,1009);
    }
}
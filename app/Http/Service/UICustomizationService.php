<?php
namespace App\Http\Service;

use Illuminate\Http\Request;
use App\Slider;

class UICustomizationService
{
	public static function getSlidersData()
	{
		try {
			$slider_data = Slider::all();
			if($slider_data) {
				return $slider_data->toArray();
			}
			return [];
		}catch(\Eexception $e) {
			throw new Exception($e->getMessage(), 1);
		}
	}
}
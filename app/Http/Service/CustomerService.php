<?php
namespace App\Http\Service;

use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Customer;
class CustomerService
{
	public static function getAuthUser()
	{
		try {
            
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response('user_not_found',404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response('token_expired',$e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response('token_invalid',$e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response('token_absent',$e->getStatusCode());
        }
        return response(compact('user'),200);
	}

    public static function getcustomerCount()
    {
        try {
            $count = Customer::all()->count();
            if($count) {
                return $count;
            }
            return 0;
        }catch(\Eexception $e) {
            throw new Exception($e->getMessage(), 1);
        }
    }
}
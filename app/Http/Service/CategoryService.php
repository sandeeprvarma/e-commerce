<?php
namespace App\Http\Service;

use Illuminate\Http\Request;
use App\Category;

class CategoryService
{
	public static function getAllCategory()
	{
		try {
			$category = Category::all();
			if($category) {
				return $category->toArray();
			}
			return [];
		}catch(\Eexception $e) {
			throw new Exception($e->getMessage(), 1);
		}
	}

	public static function getCategoryId($id)
	{
		try {
			$category = Category::where('id','=',$id)->first();
			if($category) {
				return $category->toArray();
			}
			return [];
		}catch(\Eexception $e) {
			throw new Exception($e->getMessage(), 1);
		}
	}

	public static function getCategoryByPref($id)
	{
		try {
			$category = Category::where('pref_type','=',$id)->get();
			if($category) {
				return $category->toArray();
			}
			return [];
		}catch(\Eexception $e) {
			throw new Exception($e->getMessage(), 1);
		}
	}
}
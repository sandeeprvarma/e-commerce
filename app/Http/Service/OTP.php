<?php
namespace App\Http\Service;

use Illuminate\Http\Request;
use App\Http\Service\Twilio;
use App\CustomerOtp;
use App\Customer;

class OTP
{
	public static function sendOTP($mobile)
	{
		$six_digit_random_number = mt_rand(100000, 999999);
		$twilio = Twilio::sendSms($mobile,$six_digit_random_number);
		if($twilio !== true){
			throw new \Exception('Unable to send SMS. '.$twilio);
		}
		$mobile = CommonFunction::handleCountryCode($mobile);
		$customer_otp = new CustomerOtp;
		$customer_otp->mobile = $mobile;
		$customer_otp->otp = $six_digit_random_number;
		$customer_otp->is_verified = 0;
		$customer_otp->save();
		return $customer_otp;
	}

	public static function getOTP($mobile)
	{
		$mobile = CommonFunction::handleCountryCode($mobile);
		$last_otp = CustomerOtp::where('mobile', $mobile)
						->where('created_at', '>=', \Carbon\Carbon::now()->subMinutes(5)->toDateTimeString())
						->where('is_verified' , '=', 0)
						->orderBy('created_at', 'desc')->first();
		if($last_otp) {
			return $last_otp->otp;
		}else {
			return false;
		}
	}

	public static function verifyOTP($mobile, $otp)
	{
		try {
			$mobile = CommonFunction::handleCountryCode($mobile);
			$saved_otp = self::getOTP($mobile);
			if($otp == $saved_otp) {
				CustomerOtp::where('mobile', $mobile)
						->where('otp','=',$otp)
						->update(['is_verified' => 1]);
				return true;
			}else {
				return false;
			}
		} catch (\Exception $e) {
			return $e->getMessage();
		}
	}

	public static function isVerified($mobile)
	{
		try {
			$mobile = CommonFunction::handleCountryCode($mobile);
			$is_verified = CustomerOtp::where('mobile', $mobile)
						->where('is_verified','=',1)
						->get();
			if(!$is_verified->isEmpty()) {
				return true;
			}else {
				return false;
			}
		} catch (\Exception $e) {
			return $e->getMessage();
		}
	}
}
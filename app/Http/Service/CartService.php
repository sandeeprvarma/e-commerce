<?php
namespace App\Http\Service;

use Illuminate\Http\Request;
use App\Cart;
use App\Product;

class CartService
{
	public static function getCartByCustomerId($id)
	{
		try {
			$cart = Cart::with('product')
						->where('customer_id','=',$id)->get();
			if($cart) {
				return $cart->toArray();
			}
			return [];
		}catch(\Eexception $e) {
			throw new Exception($e->getMessage(), 1);
		}
	}

	public static function addToCart($customer_id,$data)
	{
		try {
			$product = Product::find($data['product_id']);
			if(!empty($product)) {
				$cart = Cart::where('customer_id','=',$customer_id)
							->where('product_id','=',$data['product_id'])
							->first();
				if(empty($cart)){
					$cart = new Cart;
				}
				$cart->customer_id = $customer_id;
				$cart->product_id = $data['product_id'];
				$cart->quantity = $data['quantity'];
				$cart->price = $product->msrp??$product->unit_price;
				$cart->details = $data['details']??'';
				$cart->save();
				return true;
			}else {
				return false;
			}
		}catch(\Eexception $e) {
			throw new Exception($e->getMessage(), 1);
		}
	}

	public static function updateCart($id,$data)
	{
		try {
			$product = Product::find($data['product_id']);
			if(!$product->isEmpty()) {
				$cart = Cart::findOrFail($id);
				$cart->customer_id = $data['customer_id'];
				$cart->product_id = $data['product_id'];
				$cart->quantity = $data['quantity'];
				$cart->price = $product->msrp??$product->unit_price;
				$cart->details = $data['details']??'';
				$cart->save();
				return true;
			}else {
				return false;
			}
		}catch(\Eexception $e) {
			throw new Exception($e->getMessage(), 1);
		}
	}

	public static function removeFromCart($customer_id,$product_id)
	{
		try {
			$removed = Cart::where('customer_id','=', $customer_id)
							->where('product_id','=',$product_id)
							->delete();
			if($removed) {
				return true;
			}else {
				return false;
			}
		}catch(\Eexception $e) {
			throw new Exception($e->getMessage(), 1);
		}
	}

	public static function emptyCart($customer_id)
	{
		try {
			$removed = Cart::where('customer_id','=', $customer_id)->delete();
			if($removed) {
				return true;
			}else {
				return false;
			}
		}catch(\Eexception $e) {
			throw new Exception($e->getMessage(), 1);
		}
	}

	public static function getCartSummary($customer_id)
	{
		try {
			$cart_sum = array();
			$total = 0;
			$cart_data = self::getCartByCustomerId($customer_id);
			foreach ($cart_data as $key => $product) {
				$product_total = ($product['quantity']*$product['price'])-$product['product']['discount'];
				$total = $total +$product_total; 
				$cart_sum[$product['product_id']] = array('price'=>$product['price'],'discount'=>$product['product']['discount'],'quantity'=>$product['quantity'],'product_total'=>$product_total);
			}
			$cart_sum['total'] = $total;
			
			if($cart_sum) {
				return $cart_sum;
			}else {
				return [];
			}
		}catch(\Eexception $e) {
			throw new Exception($e->getMessage(), 1);
		}
	}
}
<?php
namespace App\Http\Service;

use Illuminate\Http\Request;
use App\Order;
use App\OrderDetails;
use App\Http\Service\CartService;
class OrdersService
{
	public static function getAllorders($customer_id)
	{
		try {
			$orders = Order::join('orderdetails', 'orders.id', '=', 'orderdetails.order_id')
						->join('products', 'products.id', '=', 'orderdetails.product_id')
						->where('cust_id','=',$customer_id)
						->get(['cust_id','address_id','payment_id','order_date','delivery_date','orderdetails.ship_date','sales_tax','orders.total','orderdetails.fulfilled','deleted','paid','payment_date','order_id','price','quantity','orderdetails.discount','bill_date','name','description','is_active','unit_price','unit_weight','unit_size','disc_available','disc_type','units_on_order','thumb','note']);
			if($orders) {
				return $orders->toArray();
			}
			return [];
		}catch(\Eexception $e) {
			throw new Exception($e->getMessage(), 1);
		}
	}

	public static function createOrder($customer_id,$data)
	{
		try {
			$cart = CartService::getCartSummary($customer_id);
			$order = new Order;
			$order->cust_id = $customer_id;
			$order->address_id = $data['address_id'];
			$order->payment_id= $data['payment_id']??1;
			$order->order_date = now();
			$order->delivery_date = now();
			$order->ship_date = now();
			$order->sales_tax = 0.00;
			$order->total = $cart['total'];
			$order->fulfilled = 0;
			$order->deleted = null;
			$order->paid = 0;
			$order->payment_date = now();
			$order->save();
			foreach ($cart as $product_id => $product) {
				if(is_numeric($product_id)) {
					$order_details = new OrderDetails;
					$order_details->order_id = $order->id;
					$order_details->product_id = $product_id;
					$order_details->price = $product['price'];
					$order_details->quantity = $product['quantity'];
					$order_details->discount = $product['discount']??0;
					$order_details->total = $product['product_total'];
					$order_details->bill_date = now();
					$order_details->ship_date =now();
					$order_details->fulfilled = 0;
					$order_details->salesTax =0.00;
					$order_details->save();
				}
			}
			CartService::emptyCart($customer_id);
			return true;
		}catch(\Eexception $e) {
			throw new Exception($e->getMessage(), 1);
		}
		return false;
	}

	public static function getRevenue()
	{
		try {
			$revenue = Order::whereYear('created_at',date('Y'))->get(['total'])->sum('total');
			if($revenue) {
				return $revenue;
			}
			return 0.00;
		}catch(\Eexception $e) {
			throw new Exception($e->getMessage(), 1);
		}
	}

	public static function getDailyOrders()
	{
		try {
			$orders_count = Order::whereYear('created_at',date('Y'))->get(['id'])->count();
			if($orders_count) {
				return $orders_count;
			}
			return 0;
		}catch(\Eexception $e) {
			throw new Exception($e->getMessage(), 1);
		}
	}
}
<?php
namespace App\Http\Service;

use Illuminate\Http\Request;
use App\Address;

class AddressService
{
	public static function getAllAddresses($customer_id)
	{
		try {
			$address = Address::where('customer_id','=',$customer_id)->get();
			if($address) {
				return $address->toArray();
			}
			return [];
		}catch(\Eexception $e) {
			throw new Exception($e->getMessage(), 1);
		}
	}

	public static function getAddressesByType($customer_id,$address_type)
	{
		try {
			$address = Address::where('customer_id','=',$customer_id)
							->where('address_type','=',$address_type)
							->get();
			if($address) {
				return $address->toArray();
			}
			return [];
		}catch(\Eexception $e) {
			throw new Exception($e->getMessage(), 1);
		}
	}

	public static function addAddress($data)
	{
		try {
			$address = new Address;
			$address->customer_id = $data['customer_id'];
			$address->address_type = $data['address_type'];
			$address->house_number = $data['house_number'];
			$address->block_name = $data['block_name']??'';
			$address->building_name = $data['building_name']??'';
			$address->street = $data['street']??'';
			$address->landmark = $data['landmark']??'';
			$address->zipcode = $data['zipcode']??0;
			$address->locality = $data['locality']??'';
			$address->city = $data['city']??'';
			$address->state = $data['state']??'';
			$address->country = $data['country']??'';
			$address->mobile = $data['mobile'];
			$address->is_active = $data['is_active']??1;
			$address->save();
		}catch(\Eexception $e) {
			throw new Exception($e->getMessage(), 1);
		}
	}

	public static function editAddress($id,$data)
	{
		try {

			$address = Address::findOrFail($id);
			$address->customer_id = $data['customer_id'];
			$address->address_type = $data['address_type'];
			$address->house_number = $data['house_number'];
			$address->block_name = $data['block_name']??'';
			$address->building_name = $data['building_name']??'';
			$address->street = $data['street'];
			$address->landmark = $data['landmark'];
			$address->zipcode = $data['zipcode'];
			$address->locality = $data['locality']??'';
			$address->city = $data['city'];
			$address->state = $data['state'];
			$address->country = $data['country'];
			$address->mobile = $data['mobile'];
			$address->is_active = $data['is_active']??1;
			$address->save();
		}catch(\Eexception $e) {
			throw new Exception($e->getMessage(), 1);
		}
	}
	public static function deleteAddress($id)
	{
		return Address::where('id',$id)->delete();
	}
}
<?php
namespace App\Http\Service;

use Illuminate\Http\Request;
use App\Product;
use App\Category;

class ProductService
{
	public static function getAllProducts()
	{
		try {
			$products = Product::all();
			if($products) {
				return $products->toArray();
			}
			return [];
		}catch(\Eexception $e) {
			throw new Exception($e->getMessage(), 1);
		}
	}

	public static function getProductById($id)
	{
		try {
			$products = Product::where('id','=',$id)->first();
			if($products) {
				return $products->toArray();
			}
			return [];
		}catch(\Eexception $e) {
			throw new Exception($e->getMessage(), 1);
		}
	}

	public static function getProductByCategory($cat_id)
	{
		try {
			$products = Product::where('cat_id','=',$cat_id)->get();
			if($products) {
				return $products->toArray();
			}
			return [];
		}catch(\Eexception $e) {
			throw new Exception($e->getMessage(), 1);
		}
	}

	public static function searchProducts($product_name)
	{
		try {
			$products = Product::where('name','like','%'.$product_name.'%')
								->orWhere('description','like','%'.$product_name.'%')
								->get()
								->toArray();
			$category = Category::where('name','like','%'.$product_name.'%')
								->orWhere('description','like','%'.$product_name.'%')
								->get()
								->toArray();
			$search_result = array_merge($products,$category);

			if($search_result) {
				return $search_result;
			}
			return [];
		}catch(\Eexception $e) {
			throw new Exception($e->getMessage(), 1);
		}
	}
}
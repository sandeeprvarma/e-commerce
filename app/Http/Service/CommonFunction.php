<?php
namespace App\Http\Service;
/**
 * 
 */
class CommonFunction
{
	public static function handleCountryCode($mobile)
	{
		if (strlen($mobile) == 12 && substr($mobile, 0, 2) == "91"){
		    $mobile = substr($mobile, 2, 10);
		}else if(strlen($mobile) == 13 && substr($mobile, 0, 3) == "+91") {
			$mobile = substr($mobile, 3, 10);
		}else if(preg_match('#^\+91#', $mobile)) {
			$mobile = substr($mobile, 3, 10);
		}
		return $mobile;
	}
}
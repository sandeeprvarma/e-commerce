<?php

namespace App\Http\Service;

use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Twilio\Jwt\ClientToken;

class Twilio
{
    public static function sendSms($to, $message)
    {

        $token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_sid = getenv("TWILIO_SID");
        $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
        $twilio_number = getenv("TWILIO_NUMBER");

        $client = new Client($twilio_sid, $token);
        try
        {
            // Use the client to do fun stuff like send text messages!
            $client->messages->create(
            // the number you'd like to send the message to
                $to,
                array(
                 // A Twilio phone number you purchased at twilio.com/console
                 'from' => $twilio_number,
                 // the body of the text message you'd like to send
                 'body' => $message
                )
            );
            return true;
        }catch (Twilio\Exceptions\RestException $e)
        {
            return $e->getMessage();
        }
    }
}
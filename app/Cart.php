<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
	protected $table = 'cart'; 
    protected $fillable = ['customer_id','product_id','quantity','price','details'];

    public function product()
    {
        return $this->hasOne('App\Product','id','product_id');
    }
}

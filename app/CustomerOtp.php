<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerOtp extends Model
{
    protected $table = 'customer_otp';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'otp', 'userid', 'is_verified'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'address';

    protected $fillable = ['customer_id','address_type','house_number','block_name','building_name','street','landmark','zipcode','locality','city','state','country','mobile','is_active'];

    public function customer()
    {
    	$this->belongsTo('App\Customer','customer_id');
    }
}

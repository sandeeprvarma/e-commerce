<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
	protected $table = 'product_category';
	protected $fillable = ['name','parent_id','description','thumb','is_active','pref_type'];
}
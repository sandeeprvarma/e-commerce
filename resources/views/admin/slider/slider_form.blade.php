<form method="POST" id="slider_form" action="{{ $post_url }}" enctype="multipart/form-data">
    @csrf
    @if(isset($method) && $method == 'patch')
        @method('patch')
    @endif
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Slider Name</label>
        <div class="col-sm-10">
            <input type="text" name = "name" value="{{old('name') ? old('name'): (isset($slider->name)?$slider->name:'') }}" class="form-control" placeholder="Enter Slider Name" required />
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Slider Image</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" name="image" {{$required}} />
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Slider Rank</label>
        <div class="col-sm-10">
            <input type="text" name = "rank" value="{{old('rank') ? old('rank'): (isset($slider->rank)?$slider->rank:'') }}" class="form-control" placeholder="Enter Slider Rank" required />
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2"></label>
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary m-b-0">Submit</button>
            <button type="reset" class="btn btn-warning m-b-0">Cancel</button>
        </div>
    </div>
</form>

@extends('admin.layout.app')

@section('content')
@include('admin.layout.navigation')
<div class="pcoded-content">
                    <div class="pcoded-inner-content">

                        <div class="main-body">
                            <div class="page-wrapper">
                                <div class="page-header card">
                                    <div class="card-block">
                                        <h5 class="m-b-10">Slider</h5>
                                        <ul class="breadcrumb-title b-t-default p-t-10">
                                            <li class="breadcrumb-item">
                                                <a href="#"> <i class="fa fa-home"></i> </a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="/admin/sliders">Slider</a>
                                            </li>
                                            <li class="breadcrumb-item">View Sliders
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                @if ($message = Session::get('success'))
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ $message }}</strong>
                                </div>
                                @endif
                                <div class="float-right mb-2">
                                    <a href="{{url('/admin/sliders/create')}}" class="btn btn-primary">Add Slider</a>
                                </div>
                                <div class="page-body" style="clear: both;">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>Sliders List</h5>    
                                                </div>
                                                <div class="card-block">
                                                    <div class="dt-responsive table-responsive">
                                                        <table id="simpletable" class="table table-bordered nowrap">
                                                            <thead>
                                                                <tr>
                                                                    <th>Name</th>
                                                                    <th>Image</th>
                                                                    <th>Rank</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach($sliders as $slider)
                                                                <tr>
                                                                    <td>{{$slider->name}}</td>
                                                                    <td><img src="{{url($slider->image)}}" width="50px" height="50px"></td>
                                                                    <td>{{$slider->rank}}</td>
                                                                    <td>
                                                                        <a href="{{ url('/admin/sliders/'.$slider->id).'/edit' }}" style="display: inline;"> <button class="btn btn-primary btn-outline-primary btn-icon"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                                                        </a>
                                                                        <form method="post" action="{{ url('/admin/sliders/'.$slider->id) }}" style="display: inline;">
                                                                            @method('delete')
                                                                            @csrf
                                                                            <button type="submit" class="btn btn-danger btn-outline-primary btn-icon" onclick="return confirm('Please confirm to delete?')"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                                                        </form>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>                                                            
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div id="styleSelector">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('admin.layout.app')

@section('content')
	@include('admin.layout.navigation')
	<div class="pcoded-content">
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-body">
						<div class="row">
							<div class="col-md-12 col-xl-4">
								<div class="card widget-statstic-card borderless-card">
									<div class="card-header">
										<div class="card-header-left">
											<h5>Daily orders</h5>
											<p class="p-t-10 m-b-0 text-muted"></p>
										</div>
									</div>
									<div class="card-block">
										<i class="fa fa-calendar st-icon bg-primary"></i>
										<div class="text-left">
											<h3 class="d-inline-block">{{ $data['daily_orders'] }}</h3>
											{{-- <i class="fa fa-long-arrow-up f-24 text-success m-l-15"></i> --}}
											{{-- <span class="f-right bg-success">23%</span> --}}
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-6 col-xl-4">
								<div class="card widget-statstic-card borderless-card">
									<div class="card-header">
										<div class="card-header-left">
											<h5>Customers {{date('Y')}}</h5>
											<p class="p-t-10 m-b-0 text-muted"></p>
										</div>
									</div>
									<div class="card-block">
										<i class="fa fa-users st-icon bg-warning txt-lite-color"></i>
										<div class="text-left">
											<h3 class="d-inline-block">{{$data['customer_count']}}</h3>
											{{-- <i class="fa fa-long-arrow-down text-danger f-24 m-l-15"></i> --}}
											{{-- <span class="f-right bg-danger">-5%</span> --}}
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-6 col-xl-4">
								<div class="card widget-statstic-card borderless-card">
									<div class="card-header">
										<div class="card-header-left">
											<h5>Revenue {{ date('Y') }}</h5>
											<p class="p-t-10 m-b-0 text-muted"></p>
										</div>
									</div>
									<div class="card-block">
										<i class="fa fa-line-chart st-icon bg-success"></i>
										<div class="text-left">
											<h3 class="d-inline-block">INR {{ $data['revenue'] }}</h3>
										</div>
									</div>
								</div>
							</div>



						</div>
					</div>
				</div>
				<div id="styleSelector"> </div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
@endsection
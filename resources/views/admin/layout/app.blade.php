<!DOCTYPE html>
<html lang="en">
<head>
	<title>Fresh To Pakhghar</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<link rel="icon" href="http://html.codedthemes.com/gradient-able/files/assets/images/favicon.ico" type="image/x-icon">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{asset('admin_assets/bower_components/bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('admin_assets/assets/icon/themify-icons/themify-icons.css')}}">
	<!-- <link rel="stylesheet" type="text/css" href="{{asset('admin_assets/assets/icon/font-awesome/css/font-awesome.min.css')}}"> -->
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<!-- <link rel="stylesheet" type="text/css" href="{{asset('admin_assets/assets/icon/icofont/css/icofont.css')}}"> -->
	<link rel="stylesheet" type="text/css" href="{{asset('admin_assets/assets/css/jquery.mCustomScrollbar.css') }}">
	<link rel="stylesheet" href="{{asset('admin_assets/assets/pages/chart/radial/css/radial.css')}}" type="text/css" media="all">
	<link rel="stylesheet" type="text/css" href="{{asset('admin_assets/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin_assets/assets/pages/data-table/css/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin_assets/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('admin_assets/assets/css/style.css')}}">
</head>
<body>
	<div class="theme-loader">
		<div class="loader-track">
			<div class="loader-bar"></div>
		</div>
	</div>
	@yield('content')
	<script src="{{asset('admin_assets/bower_components/jquery/js/jquery.min.js')}}"></script>
	<script src="{{asset('admin_assets/bower_components/jquery-ui/js/jquery-ui.min.js')}}"></script>
	<script src="{{asset('admin_assets/bower_components/popper.js/js/popper.min.js')}}"></script>
	<script src="{{asset('admin_assets/bower_components/bootstrap/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('admin_assets/assets/pages/widget/excanvas.js')}}"></script>
	<script src="{{asset('admin_assets/bower_components/jquery-slimscroll/js/jquery.slimscroll.js')}}"></script>
	<script src="{{asset('admin_assets/bower_components/modernizr/js/modernizr.js')}}"></script>
	<script src="{{asset('admin_assets/assets/js/SmoothScroll.js')}}"></script>
	<script src="{{asset('admin_assets/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
	<script src="{{asset('admin_assets/bower_components/chart.js/js/Chart.js')}}"></script>
	<script src="{{asset('admin_assets/assets/pages/widget/amchart/amcharts.js')}}"></script>
	<script src="{{asset('admin_assets/assets/pages/widget/amchart/serial.js')}}"></script>
	<script src="{{asset('admin_assets/assets/pages/widget/amchart/light.js')}}"></script>
	<script src="{{ asset('admin_assets/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{ asset('admin_assets/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
	<script src="{{asset('admin_assets/assets/pages/data-table/js/data-table-custom.js')}}"></script>
	<script src="{{asset('admin_assets/assets/js/pcoded.min.js')}}"></script>
	<script src="{{asset('admin_assets/assets/js/vertical/vertical-layout.min.js')}}"></script>
	<script src="{{asset('admin_assets/assets/pages/dashboard/custom-dashboard.js')}}"></script>
	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
	<script src="https://cdn.tiny.cloud/1/exyq0045blxuaw67jnn3zw0666ht6geqq6eaj0geti9ipz8g/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
	<!-- <script src="{{asset('admin_assets/assets/pages/wysiwyg-editor/js/tinymce.min.js')}}"></script>
	<script src="{{asset('admin_assets/assets/pages/wysiwyg-editor/wysiwyg-editor.js')}}"></script> -->
	<script src="{{asset('admin_assets/assets/js/script.js')}}"></script>
</body>
</html>
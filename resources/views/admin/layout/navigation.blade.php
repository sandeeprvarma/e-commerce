<div id="pcoded" class="pcoded">
	<div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">
        <nav class="navbar header-navbar pcoded-header">
            <div class="navbar-wrapper">
                <div class="navbar-logo">
                    <a class="mobile-menu" id="mobile-collapse" href="#!">
                        <i class="ti-menu"></i>
                    </a>
                    <div class="mobile-search">
                        <div class="header-search">
                            <div class="main-search morphsearch-search">
                                <div class="input-group">
                                    <span class="input-group-addon search-close"><i class="ti-close"></i></span>
                                    <input type="text" class="form-control" placeholder="Enter Keyword">
                                    <span class="input-group-addon search-btn"><i class="ti-search"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="/admin" style="font-family: Poppins;font-weight: 700;font-size: 18px;">
                        FTP Admin
                    </a>
                    <a class="mobile-options">
                        <i class="ti-more"></i>
                    </a>
                </div>
                <div class="navbar-container container-fluid">
                    <ul class="nav-right">
                        <li class="user-profile header-notification">
                            <a href="#!">
                                {{-- <img src="{{ asset('admin_assets/assets/images/avatar-4.jpg') }}" class="img-radius" alt="User-Profile-Image"> --}}
                                <span>{{ Auth::user()->name }}</span>
                                <i class="ti-angle-down"></i>
                            </a>
                            <ul class="show-notification profile-notification">                                
                                {{-- <li>
                                    <a href="#">
                                        <i class="ti-user"></i> Profile
                                    </a>
                                </li> --}}                                
                                <li>
                                    <a href="{{ url('logout') }}">
                                        <i class="ti-layout-sidebar-left"></i> Logout
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                <nav class="pcoded-navbar">
					<div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
					<div class="pcoded-inner-navbar main-menu">
						<ul class="pcoded-item pcoded-left-item">
							<li class="active">
								<a href="/admin">
									<span class="pcoded-micon"><i class="ti-home"></i><b>D</b></span>
									<span class="pcoded-mtext">Dashboard</span>
									<span class="pcoded-mcaret"></span>
								</a>
							</li>
						</ul>
						<ul class="pcoded-item pcoded-left-item">
							<li class="">
								<a href="/admin/customers">
									<span class="pcoded-micon"><i class="ti-user"></i><b>C</b></span>
									<span class="pcoded-mtext">Customers</span>
									<span class="pcoded-mcaret"></span>
								</a>
							</li>
						</ul>
						<ul class="pcoded-item pcoded-left-item">
							<li class="">
								<a href="/admin/product-category">
									<span class="pcoded-micon"><i class="ti-layout-list-thumb-alt"></i><b>PC</b></span>
									<span class="pcoded-mtext">Product Category</span>
									<span class="pcoded-mcaret"></span>
								</a>
							</li>
						</ul>
						<ul class="pcoded-item pcoded-left-item">
							<li class="">
								<a href="/admin/product">
									<span class="pcoded-micon"><i class="ti-shopping-cart"></i><b>P</b></span>
									<span class="pcoded-mtext">Products</span>
									<span class="pcoded-mcaret"></span>
								</a>
							</li>
						</ul>
						<ul class="pcoded-item pcoded-left-item">
							<li class="">
								<a href="/admin/orders">
									<span class="pcoded-micon"><i class="ti-truck"></i><b>OD</b></span>
									<span class="pcoded-mtext">Orders</span>
									<span class="pcoded-mcaret"></span>
								</a>
							</li>
						</ul>
						<ul class="pcoded-item pcoded-left-item">
							<li class="">
								<a href="/admin/sliders">
									<span class="pcoded-micon"><i class="ti-layers-alt"></i><b>S</b></span>
									<span class="pcoded-mtext">Sliders</span>
									<span class="pcoded-mcaret"></span>
								</a>
							</li>
						</ul>
						<!-- <div class="pcoded-navigation-label">Dashboard</div>
						<ul class="pcoded-item pcoded-left-item">
							<li class="active">
								<a href="javascript:void(0);">
									<span class="pcoded-micon"><i class="ti-home"></i><b>D</b></span>
									<span class="pcoded-mtext">Dashboard</span>
									<span class="pcoded-mcaret"></span>
								</a>
							</li>
						</ul>
						<div class="pcoded-navigation-label">Products</div>
						<ul class="pcoded-item pcoded-left-item">
							<li class="pcoded-hasmenu">
								<a href="javascript:void(0);">
									<span class="pcoded-micon"><i class="ti-home"></i><b>P</b></span>
									<span class="pcoded-mtext">Product Category</span>
									<span class="pcoded-mcaret"></span>
								</a>
								<ul class="pcoded-submenu">
									<li class="">
										<a href="/admin/add-category">
											<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
											<span class="pcoded-mtext">Add Product Category</span>
											<span class="pcoded-mcaret"></span>
										</a>
									</li>
									<li class="">
										<a href="/admin/view-category">
											<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
											<span class="pcoded-mtext">View Product Category</span>
											<span class="pcoded-mcaret"></span>
										</a>
									</li>
								</ul>
							</li>
						</ul>
						<ul class="pcoded-item pcoded-left-item">
							<li class="pcoded-hasmenu">
								<a href="javascript:void(0);">
									<span class="pcoded-micon"><i class="ti-home"></i><b>P</b></span>
									<span class="pcoded-mtext">Products</span>
									<span class="pcoded-mcaret"></span>
								</a>
								<ul class="pcoded-submenu">
									<li class="">
										<a href="/admin/add-category">
											<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
											<span class="pcoded-mtext">Add Product</span>
											<span class="pcoded-mcaret"></span>
										</a>
									</li>
									<li class="">
										<a href="/admin/view-category">
											<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
											<span class="pcoded-mtext">View Products</span>
											<span class="pcoded-mcaret"></span>
										</a>
									</li>
								</ul>
							</li>
						</ul>
						<div class="pcoded-navigation-label">Orders</div>
						<ul class="pcoded-item pcoded-left-item">
							<li class="">
								<a href="javascript:void(0);">
									<span class="pcoded-micon"><i class="ti-home"></i><b>OD</b></span>
									<span class="pcoded-mtext">Orders</span>
									<span class="pcoded-mcaret"></span>
								</a>
							</li>
						</ul> -->
					</div>
				</nav>
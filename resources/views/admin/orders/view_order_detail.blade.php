@extends('admin.layout.app')
@section('content')
	@include('admin.layout.navigation')
	<div class="pcoded-content">
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header card">
						<div class="card-block">
							<h5 class="m-b-10">Orders</h5>
							<ul class="breadcrumb-title b-t-default p-t-10">
								<li class="breadcrumb-item">
									<a href="index-2.html"> <i class="fa fa-home"></i> </a>
								</li>
								<li class="breadcrumb-item">
									<a href="#">Orders</a>
								</li>
								 <li class="breadcrumb-item">
								 	<a href="#!">View Orders</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="page-body">
						<div class="container">
                            <div>
                                <div class="card">
                                    <div class="row invoice-contact">
                                        <div class="col-md-8">
                                            <div class="invoice-box row">
                                                <div class="col-sm-12">
                                                    <table class="table table-responsive invoice-table table-borderless">
                                                    <tbody>
                                                        <!-- <tr>
                                                        <td><img src="../files/assets/images/logo-blue.png" class="m-b-10" alt=""></td>
                                                        </tr> -->
                                                        <tr>
                                                            <td><h6>Freshly</h6></td>
                                                        </tr>
                                                        <tr>
                                                            <td>208, Paris Point, Varachha Road, Bangladesh. (1234) - 567891</td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="mailto:demo@gmail.com" target="_top">freshly@gmail.com</a>
                                                        </td>
                                                        </tr>
                                                        <tr>
                                                            <td>+91 8888-888-888</td>
                                                        </tr>
                                                    </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                        </div>
                                    </div>
                                    <div class="card-block">
                                        <div class="row invoive-info">
                                            <div class="col-md-4 col-xs-12 invoice-client-info">
                                                <h6>Client Information :</h6>
                                                <h6 class="m-0">{{$orders[0]->first_name.' '.$orders[0]->last_name}}</h6>
                                                <p class="m-0 m-t-10">{{$orders[0]->house_number.','.$orders[0]->building_name}}</p>
                                                <p class="m-0">{{$orders[0]->std_code.' '.$orders[0]->mobile}}</p>
                                                <p>{{$orders[0]->email}}</p>
                                            </div>
                                            <div class="col-md-4 col-sm-6">
                                            <h6>Order Information :</h6>
                                            <table class="table table-responsive invoice-table invoice-order table-borderless">
                                                <tbody>
                                                    <tr>
                                                        <th>Date :</th>
                                                        <td>{{date("d-m-Y", strtotime($orders[0]->order_date)) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Status :</th>
                                                        <td>
                                                            @if($orders[0]->fulfilled)
                                                                <span class="label label-success">Delivered</span>
                                                            @else
                                                                <span class="label label-warning">Pending</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Id :</th>
                                                        <td>
                                                            {{'#'.$orders[0]->id}}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            </div>
                                            <div class="col-md-4 col-sm-6">
                                                <h6 class="m-b-20">Total Due: <span>{{'Rs.'.$orders[0]->total}}</span></h6>
                                                <!-- <h6 class="text-uppercase text-primary">Total Due :
                                                <span>{{'Rs.'.$orders[0]->total}}</span>
                                                </h6> -->
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="table-responsive">
                                                    <table class="table  invoice-detail-table">
                                                        <thead>
                                                            <tr class="thead-default">
                                                                <th>Product Name</th>
                                                                <th>Image</th>          
                                                                <th>Quantity</th>
                                                                <th>Amount</th>
                                                                <th>Total</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($order_details as $order)             
                                                            <tr>
                                                                <td>
                                                                    <h6>{{$order->name}}</h6>
                                                                </td>
                                                                <td class="pro-list-img" tabindex="0">
                                                                    <img src="{{url($order->thumb)}}" class="img-fluid" alt="tbl">
                                                                <!-- <h6>Logo Design</h6> -->
                                                                <!-- <p>lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </p> -->
                                                                </td>          
                                                                <td>{{$order->quantity}}</td>
                                                                <td>{{$order->price}}</td>
                                                                <td>{{$order->total}}</td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <table class="table table-responsive invoice-table invoice-total">
                                                    <tbody>
                                                        <tr>
                                                            <th>Sub Total :</th>
                                                            <td>{{'Rs.'.$orders[0]->total}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Taxes ({{$orders[0]->sales_tax}}%) :</th>
                                                            <td>Rs.0</td>
                                                        </tr>
                                                        {{-- <tr>
                                                            <th>Discount ({{$orders->discount}}%) :</th>
                                                            <td>$45.00</td>
                                                        </tr> --}}
                                                        <tr class="text-info">
                                                            <td>
                                                                <hr>
                                                                <h5 class="text-primary">Total :</h5>
                                                            </td>
                                                            <td>
                                                                <hr>
                                                                <h5 class="text-primary">{{'Rs.'.$orders[0]->total}}</h5>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- <div class="row">
                                            <div class="col-sm-12">
                                                <h6>Terms And Condition :</h6>
                                                <p>lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor </p>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>

                                <!-- <div class="row text-center">
                                    <div class="col-sm-12 invoice-btn-group text-center">
                                        <button type="button" class="btn btn-primary btn-print-invoice m-b-10 btn-sm waves-effect waves-light m-r-20">Print</button>
                                        <button type="button" class="btn btn-danger waves-effect m-b-10 btn-sm waves-light">Cancel</button>
                                    </div>
                                </div> -->
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
@endsection
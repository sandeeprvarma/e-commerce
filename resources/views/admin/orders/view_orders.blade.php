@extends('admin.layout.app')
@section('content')
	@include('admin.layout.navigation')
	<div class="pcoded-content">
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header card">
						<div class="card-block">
							<h5 class="m-b-10">Orders</h5>
							<ul class="breadcrumb-title b-t-default p-t-10">
								<li class="breadcrumb-item">
									<a href="index-2.html"> <i class="fa fa-home"></i> </a>
								</li>
								<li class="breadcrumb-item">
									<a href="#">Orders</a>
								</li>
								 <li class="breadcrumb-item">
								 	<a href="#!">View Orders</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="page-body">						
						<!-- Datatable Start-->
						<div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Orders List</h5>        
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="simpletable" class="table table-bordered nowrap">
                                                <thead>
                                                    <tr>
                                                        <th>Order ID</th>
                                                        <th>Customer</th>
                                                        <th>Status</th>
                                                        <th>Delivery Date</th>
                                                        <th>Payment Method</th>
                                                        <th>Total</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($orders as $order)
                                                	<tr>                
                                                		<td>{{'#'.$order->id}}</td>
                                                		<td>{{$order->first_name.' '.$order->last_name}}</td>
                                                        @if($order->fulfilled)
                                                	       <td><span class="label label-success">Delivered</span></td>
                                                        @else
                                                            <td><span class="label label-warning">Pending</span></td>
                                                        @endif
                                                		<td>{{date("d-m-Y", strtotime($order->delivery_date)) }}</td>
                                                        <td>Cash On Delivery</td>
                                                		<td>{{'Rs.'.$order->total}}</td>
                                                		<td>
                                                            <form method="post" action="{{ url('/admin/orders/'.$order->id) }}" style="display: inline;">
                                                                @method('get')
                                                                @csrf
                                                                 <button class="btn btn-primary btn-outline-primary"><i class="fa fa-eye"></i></button>
                                                            </form>

                                                            <!-- <a href="{{ url('/admin/orders/'.$order->id).'/edit' }}" style="display: inline;">
                                                                <button class="btn btn-primary btn-outline-primary"><i class="icofont icofont-user-alt-3"></i></button>
                                                            </a> -->
                                                			
                                                            <form method="post" action="{{ url('/admin/orders/'.$order->id) }}" style="display: inline;">
                                                                @method('delete')
                                                                @csrf
                                                			     <button class="btn btn-danger btn-outline-danger"><i class="ti-trash"></i></button>
                                                             </form>
                                                		</td>                   
                                                	</tr>
                                                    @endforeach
                                                </tbody>                                                        
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<!-- Datatable End-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
@endsection
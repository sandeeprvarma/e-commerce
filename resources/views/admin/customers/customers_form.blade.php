<form method="POST" action="{{ $post_url }}" enctype="multipart/form-data">
    @csrf
    @if(isset($method) && $method == 'PATCH')
        @method('PATCH')
    @endif
    <div class="form-group row">
        <label class="col-sm-2 col-form-label"><span style="color:#F00;font-weight:bold;">* </span>First Name</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="first_name" placeholder="Enter First Name" autocomplete="off" value="{{old('first_name') ? old('first_name'): (isset($customer->first_name)?$customer->first_name:'')}}" required>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label"><span style="color:#F00;font-weight:bold;">* </span>Last Name</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="last_name" placeholder="Enter Last Name" autocomplete="off" value="{{old('last_name') ? old('last_name'): (isset($customer->last_name)?$customer->last_name:'')}}" required>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label"><span style="color:#F00;font-weight:bold;">* </span>Username</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="username" placeholder="Enter Username" autocomplete="off" value="{{old('username') ? old('username'): (isset($customer->username)?$customer->username:'')}}" required>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label"><span style="color:#F00;font-weight:bold;">* </span>Email</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="email" placeholder="Enter Email" autocomplete="off" value="{{old('email') ? old('email'): (isset($customer->email)?$customer->email:'')}}" required>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label"><span style="color:#F00;font-weight:bold;">* </span>Mobile</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="mobile" placeholder="Enter Mobile Number" autocomplete="off" value="{{old('mobile') ? old('mobile'): (isset($customer->mobile)?$customer->mobile:'')}}" required>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Status</label>
        <div class="col-sm-10">
            <select name="is_active" class="form-control">
                <option value="1" {{isset($customer) && $customer->is_active == 1? 'selected':''}}>Yes</option>
                <option value="0" {{isset($customer) && $customer->is_active == 0? 'selected':''}}>No</option>
            </select>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2"></label>
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary m-b-0">Submit</button>
            <button type="reset" class="btn btn-warning m-b-0">Cancel</button>
        </div>
    </div>
</form>
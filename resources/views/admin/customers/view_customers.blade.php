@extends('admin.layout.app')
@section('content')
	@include('admin.layout.navigation')
	<div class="pcoded-content">
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header card">
						<div class="card-block">
							<h5 class="m-b-10">Customers</h5>
							<ul class="breadcrumb-title b-t-default p-t-10">
								<li class="breadcrumb-item">
									<a href="index-2.html"> <i class="fa fa-home"></i> </a>
								</li>
								<li class="breadcrumb-item">
									<a href="/view_prod_cat">Customers</a>
								</li>
								 <li class="breadcrumb-item">
								 	<a href="#!">View Customers</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="page-body">						
						<!-- Datatable Start-->
						<div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Customers List</h5>        
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="simpletable" class="table table-bordered nowrap">
                                                <thead>
                                                    <tr>
                                                        <th>First Name</th>
                                                        <th>Last Name</th>
                                                        <th>Username</th>
                                                        <th>Email</th>
                                                        <th>Mobile</th>
                                                        <th>Active</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($customers as $customer)
                                                	<tr>                
                                                		<td>{{$customer->first_name}}</td>
                                                		<td>{{$customer->last_name}}</td>
                                                		<td>{{$customer->username}}</td>
                                                		<td style="word-break: break-word;white-space: normal !important;">{{$customer->email}}</td>
                                                        <td>{{$customer->std_code.' '. $customer->mobile}}</td>
                                                		<td>{{$customer->is_active?'Yes':'No'}}</td>             		
                                                		<td>
                                                            <a href="{{ url('/admin/customers/'.$customer->id).'/edit' }}" style="display: inline;">
                                                                <button class="btn btn-primary btn-outline-primary"><i class="ti-pencil-alt"></i></button>
                                                            </a>
                                                			
                                                            <form method="post" action="{{ url('/admin/customers/'.$customer->id) }}" style="display: inline;">
                                                                @method('delete')
                                                                @csrf
                                                			     <button class="btn btn-danger btn-outline-danger"><i class="ti-trash"></i></button>
                                                             </form>
                                                		</td>                   
                                                	</tr>
                                                    @endforeach
                                                </tbody>                                                        
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<!-- Datatable End-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
@endsection
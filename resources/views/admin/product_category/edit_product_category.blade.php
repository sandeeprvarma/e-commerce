@extends('admin.layout.app')

@section('content')
	@include('admin.layout.navigation')
	<div class="pcoded-content">
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header card">
						<div class="card-block">
							<h5 class="m-b-10">Edit Product Category</h5>
							<ul class="breadcrumb-title b-t-default p-t-10">
								<li class="breadcrumb-item">
									<a href="index-2.html"> <i class="fa fa-home"></i> </a>
								</li>
								<li class="breadcrumb-item">
									<a href="/view_prod_cat">Product Category</a>
								</li>
								 <li class="breadcrumb-item">
								 	Edit Product Category
								</li>
							</ul>
						</div>
					</div>
					<div class="page-body">						
						<!-- Datatable Start-->
						<div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-block">
                                        <h4 class="sub-title">Product Category Details</h4>
                                        @if ($errors->any())
	                                        <div class="alert alert-danger">
	                                            <ul>
	                                                @foreach ($errors->all() as $error)
	                                                    <li>{{ $error }}</li>
	                                                @endforeach
	                                            </ul>
	                                        </div>
	                                    @endif
	                                    @if ($message = Session::get('success'))
	                                    <div class="alert alert-success alert-block">
	                                        <button type="button" class="close" data-dismiss="alert">×</button>
	                                            <strong>{{ $message }}</strong>
	                                    </div>
	                                    @endif
                                        @include('admin.product_category.product_category_form',['post_url'=>'/admin/product-category/'.$product->id, 'method' => 'PATCH', 'required' => ''])
                                    </div>
                                </div>
                            </div>
                        </div>
						<!-- Datatable End-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
@endsection
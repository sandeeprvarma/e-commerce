<form method="POST" id="prod_cat" action="{{ $post_url }}" enctype="multipart/form-data">
    @csrf
    @if(isset($method) && $method == 'PATCH')
        @method('PATCH')
    @endif
    <div class="form-group row">
        <label class="col-sm-2 col-form-label"><span style="color:#F00;font-weight:bold;">* </span>Category Name</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="name" placeholder="Enter Product Category Name" autocomplete="off" value="{{old('name') ? old('name'): (isset($product->name)?$product->name:'')}}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Description</label>
        <div class="col-sm-10">
            <textarea class="form-control" name="description">{{old('description') ? old('description'): (isset($product->description)?$product->description:'')}}</textarea>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Parent</label>
        <div class="col-sm-10">
            <select name="parent" class="form-control">
                <option value="0">-- NONE --</option>
                @foreach($parents as $parent)
                <option value="{{$parent->id}}" {{(isset($product) && $product->id == $parent->parent_id)?'selected':''}}>{{$parent->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label"><span style="color:#F00;font-weight:bold;">* </span>Image</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" name="thumb" {{$required}} />
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Status</label>
        <div class="col-sm-10">
            <select name="status" class="form-control">
                <option value="1" {{isset($product) && $product->is_active == 1? 'selected':''}}>Yes</option>
                <option value="0" {{isset($product) && $product->is_active == 0? 'selected':''}}>No</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Preference</label>
        <div class="col-sm-10">
            <select name="pref" class="form-control">
                <option value="1" {{isset($product) && $product->pref_type == 1? 'selected':''}}>Vegetarian</option>
                <option value="2" {{isset($product) && $product->pref_type == 2? 'selected':''}}>Non-Vegetarian</option>
            </select>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2"></label>
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary m-b-0">Submit</button>
            <button type="reset" class="btn btn-warning m-b-0">Cancel</button>
        </div>
    </div>
</form>
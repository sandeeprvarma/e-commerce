@extends('admin.layout.app')
@section('content')
	@include('admin.layout.navigation')
	<div class="pcoded-content">
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header card">
						<div class="card-block">
							<h5 class="m-b-10">Product Category</h5>
							<ul class="breadcrumb-title b-t-default p-t-10">
								<li class="breadcrumb-item">
									<a href="index-2.html"> <i class="fa fa-home"></i> </a>
								</li>
								<li class="breadcrumb-item">
									<a href="/view_prod_cat">Product Category</a>
								</li>
								 <li class="breadcrumb-item">
								 	<a href="#!">View Categories</a>
								</li>
							</ul>
						</div>
					</div>
                    <div class="float-right mb-2">
                        <a href="{{url('/admin/product-category/create')}}" class="btn btn-primary">Add Product Category</a>
                    </div>
					<div class="page-body" style="clear: both;">						
						<!-- Datatable Start-->
						<div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Product Categories List</h5>        
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="simpletable" class="table table-bordered nowrap">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Parent</th>
                                                        <th>Description</th>
                                                        <th>Thumb</th>
                                                        <th>Status</th>
                                                        <th>Preference</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($categories as $category)
                                                	<tr>           
                                                		<td>{{$category->name}}</td>
                                                		<td>{{$category->parent_id}}</td>
                                                		<td style="word-break: break-word;white-space: normal !important;">{{$category->description}}</td>
                                                          <td class="pro-list-img" tabindex="0">
                                                            <img style="width:100px;" src="{{url($category->thumb)}}" class="img-fluid" alt="tbl">
                                                        </td>
                                                        @if($category->is_active)
                                                		  <td><span class="label label-success">Active</span></td>
                                                        @else
                                                            <td><span class="label label-danger">Inactive</span></td>
                                                        @endif
                                                		<td>{{$category->pref_type?'Vegetarian': 'Non Vegetarian'}}</td>
                                                		<td>
                                                            <a href="{{ url('/admin/product-category/'.$category->id).'/edit' }}" style="display: inline;">
                                                                <button class="btn btn-primary btn-outline-primary"><i class="ti-pencil-alt"></i></button>
                                                            </a>
                                                			
                                                            <form method="post" action="{{ url('/admin/product-category/'.$category->id) }}" style="display: inline;">
                                                                @method('delete')
                                                                @csrf
                                                			     <button class="btn btn-danger btn-outline-danger"><i class="ti-trash"></i></button>
                                                             </form>
                                                		</td>
                                                	</tr>
                                                    @endforeach
                                                </tbody>                                                        
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<!-- Datatable End-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
@endsection
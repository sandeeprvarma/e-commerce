<form method="POST" id="prod_form" action="{{$post_url}}" enctype="multipart/form-data">
    @csrf
    @if(isset($method) && $method == 'PATCH')
        @method('PATCH')
    @endif
    <div class="form-group row">
        <label class="col-sm-2 col-form-label"><span style="color:#F00;font-weight:bold;">* </span>Product Name</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="name" value="{{old('name') ? old('name'): (isset($product->name)?$product->name:'') }}" placeholder="Enter Product Name" autocomplete="off">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label"><span style="color:#F00;font-weight:bold;">* </span>Category</label>
        <div class="col-sm-10">
            <select name="cat_id" class="form-control" >
                <option value="">-- Select Category --</option>
                @foreach($categories as $category)
                <option value="{{$category->id}}" {{(isset($product) && $product->cat_id == $category->id)?'selected':''}}>{{$category->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label"><span style="color:#F00;font-weight:bold;">* </span>Description</label>
        <div class="col-sm-10">
            <textarea class="form-control" name="description" autocomplete="off" required>{{old('description') ? old('description'): (isset($product->description)?$product->description:'') }}</textarea>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label"><span style="color:#F00;font-weight:bold;">* </span>Product Price</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="unit_price" value="{{old('unit_price') ? old('unit_price'): (isset($product->unit_price)?$product->unit_price:'') }}" placeholder="Enter Product Price" autocomplete="off">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label"><span style="color:#F00;font-weight:bold;">* </span>Product Quantity</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="stock" value="{{old('stock') ? old('stock'): (isset($product->units_in_stocks)?$product->units_in_stocks:'') }}" placeholder="Enter Product Quantity/ Units In Stock" autocomplete="off">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label"><span style="color:#F00;font-weight:bold;">* </span> Product Image</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" name="thumb" {{$required}}/>
        </div>
    </div>
    @if(!isset($product))
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Additional Images</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" name="add_thumb[]" multiple/>
        </div>
    </div>
    @endif
    <div class="form-group row">
        <label class="col-sm-2 col-form-label"><span style="color:#F00;font-weight:bold;">* </span>Product Weight</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="unit_weight" value="{{old('unit_weight') ? old('unit_weight'): (isset($product->unit_weight)?$product->unit_weight:'') }}" placeholder="Enter Product Weight" autocomplete="off">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Weight Class</label>
        <div class="col-sm-10">
            <select name="weight_class" class="form-control">
                <option value="1" {{isset($product) && $product->weight_class == 1? 'selected':''}}>Grams</option>
                <option value="2" {{isset($product) && $product->weight_class == 2? 'selected':''}}>Kilogram</option>
                <option value="2" {{isset($product) && $product->weight_class == 3? 'selected':''}}>Dozen</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Status</label>
        <div class="col-sm-10">
            <select name="is_active" class="form-control">
                <option value="1" {{isset($product) && $product->is_active == 1? 'selected':''}}>Yes</option>
                <option value="0" {{isset($product) && $product->is_active == 0? 'selected':''}}>No</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Discount Available</label>
        <div class="col-sm-10">
            <select name="disc_avail" id="disc_avail" class="form-control">
                <option value="1" {{isset($product) && $product->disc_available == 1? 'selected':''}}>Yes</option>
                <option value="0" {{isset($product) && $product->disc_available == 0? 'selected':''}}>No</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Discount Type</label>
        <div class="col-sm-10">
            <select name="disc_type" id="disc_type" {{isset($product) && $product->disc_available == 0? 'disabled':''}} class="form-control">
                <option value="1" {{isset($product) && $product->disc_type == 1? 'selected':''}}>Percentage</option>
                <option value="0" {{isset($product) && $product->disc_type == 0? 'selected':''}}>Rupees</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Discount in Rs / %</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="discount" value="{{old('discount') ? old('discount'): (isset($product->discount)?$product->discount:'') }}" placeholder="Enter Discount in Rupees OR Percentage" autocomplete="off">
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2"></label>
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary m-b-0">Submit</button>
            <button type="reset" class="btn btn-warning m-b-0">Cancel</button>
        </div>
    </div>
</form>
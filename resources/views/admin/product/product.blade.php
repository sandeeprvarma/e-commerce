@extends('admin.layout.app')

@section('content')
	@include('admin.layout.navigation')
	<div class="pcoded-content">
		<div class="pcoded-inner-content">
			<div class="main-body">
				<div class="page-wrapper">
					<div class="page-header card">
						<div class="card-block">
							<h5 class="m-b-10">Products</h5>
							<ul class="breadcrumb-title b-t-default p-t-10">
								<li class="breadcrumb-item">
									<a href="index-2.html"> <i class="fa fa-home"></i> </a>
								</li>
								<li class="breadcrumb-item">
									<a href="/view_prod">Product</a>
								</li>
								 <li class="breadcrumb-item">
								 	View Products
								</li>
							</ul>
						</div>
					</div>
                    <div class="float-right mb-2">
                        <a href="/admin/product/create" class="btn btn-primary">Add Product</a>
                    </div>
					<div class="page-body" style="clear: both;">						
						<!-- Datatable Start-->
						<div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Product Categories List</h5>        
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="simpletable" class="table table-bordered nowrap">
                                                <thead>
                                                    <tr>
                                                        <th>Product Image</th>
                                                        <th>Product Name</th>
                                                        <th>Price</th>
                                                        <th>Quantity</th>
                                                        <th>Status</th>                  
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($products as $product)
                                                	<tr>
                                                        <td class="pro-list-img" tabindex="0">
                                                            <img style="width:100px;" src="{{url($product->thumb)}}" class="img-fluid" alt="tbl">
                                                        </td>
                                                		<td>{{$product->name}}</td>
                                                		<td>{{$product->unit_price}}</td>
                                                		<td>{{$product->units_in_stocks}}</td>
                                                		<td>{{$product->is_active?'Active':'In Active'}}</td>
                                                		<td>
                                                            <a href="{{ url('/admin/product/'.$product->id).'/edit' }}" style="display: inline;">
                                                                <button class="btn btn-primary btn-outline-primary"><i class="ti-pencil-alt"></i></button>
                                                            </a>
                                                            
                                                            <form method="post" action="{{ url('/admin/product/'.$product->id) }}" style="display: inline;">
                                                                @method('delete')
                                                                @csrf
                                                                 <button class="btn btn-danger btn-outline-danger"><i class="ti-trash"></i></button>
                                                             </form>
                                                        </td>
                                                	</tr>
                                                    @endforeach       
                                                </tbody>                                                        
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<!-- Datatable End-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
@endsection
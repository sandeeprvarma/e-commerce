<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('cust_id');
            $table->tinyInteger('address_id');
            $table->bigInteger('payment_id');
            $table->date('order_date');
            $table->date('delivery_date');
            $table->date('ship_date');
            $table->float('sales_tax',8,2)->default('0.00');
            $table->float('total',8,2);
            $table->tinyInteger('fulfilled')->default('0');
            $table->tinyInteger('deleted')->nullable();
            $table->integer('paid')->default('0');
            $table->date('payment_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}

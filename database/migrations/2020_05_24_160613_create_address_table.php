<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address', function (Blueprint $table) {
            $table->id();
            $table->integer('customer_id');
            $table->tinyInteger('address_type');
            $table->string('house_number');
            $table->string('block_name');
            $table->string('building_name');
            $table->string('street');
            $table->string('landmark');
            $table->bigInteger('zipcode');
            $table->string('locality');
            $table->string('city');
            $table->string('state');
            $table->string('country');
            $table->bigInteger('mobile');
            $table->tinyInteger('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('address');
    }
}

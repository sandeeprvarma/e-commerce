<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFtpProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->bigInteger('cat_id');
            $table->tinyInteger('is_active');
            $table->float('unit_price', 8, 2);
            $table->integer('unit_weight');
            $table->string('unit_weight_class');
            $table->string('unit_size');            
            $table->integer('units_in_stocks');
            $table->float('msrp', 8, 2);
            $table->tinyInteger('disc_available');
            $table->tinyInteger('disc_type');
            $table->float('discount', 8, 2);
            $table->tinyInteger('units_on_order');
            $table->tinyInteger('reorder_level');
            $table->text('thumb');
            $table->integer('ranking');
            $table->text('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ftp_products');
    }
}

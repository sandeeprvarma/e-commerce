<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
    	foreach (range(1,10) as $index) {
	        DB::table('customers')->insert([
	            'username' => $faker->name,
	            'email' => $faker->email,
	            'password' => bcrypt('secret'),
	            'first_name' => $faker->firstName,
	            'last_name' => $faker->lastName,
	            'std_code' => '+91',
	            'mobile' => $faker->numberBetween(8000000000,9000000000),
	            'is_active' => 0
	        ]);
		}
    }
}

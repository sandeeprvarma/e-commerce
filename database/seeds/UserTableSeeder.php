<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'name' => 'Abhimanyu Bind',
        	'email' => 'manub145@gmail.com',
        	'user_type' => 1,
        	'password' => bcrypt('Ab123456')
        ]);
    }
}

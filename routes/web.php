<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');
Route::group(['prefix' => 'admin','middleware' => ['check_admin'],'namespace' => 'Admin'], function() {
	// Route::get('/', function () {
	//     return view('admin.dashboard');
	// });
	Route::get('', 'HomeController@index');

	Route::resource('product-category','ProductCategoryController');
	Route::resource('sliders','SliderController');
	Route::resource('product','ProductController');
	Route::resource('customers','CustomersController');
	Route::resource('orders','OrdersController');

	/*Route::get('/', 'ManageCategoryController@index');
	Route::get('/add-category', 'ManageCategoryController@create');
	Route::post('/add-category', 'ManageCategoryController@store');
	Route::get('/view-category', 'ManageCategoryController@index');
	Route::get('/edit-category/{id}', 'ManageCategoryController@edit');
	Route::post('/edit-category/{id}', 'ManageCategoryController@update');
	Route::get('/delete-category/{id}', 'ManageCategoryController@destroy');

	Route::resource('restaurant-category','RestaurantsCategoryController');
	Route::resource('restaurant','RestaurantsController');
	Route::resource('suppliers-category','SupplierCategoryController');
	Route::resource('suppliers','SupplierController');
	Route::resource('sliders','SliderController');

	Route::get('/import_excel/{table}', 'ImportExcelController@importForm');
	Route::post('/import_excel/{table}', 'ImportExcelController@import');

	Route::post('/upload-images', 'ImportExcelController@uploadImages');

	Route::get('/bookings', 'BookingsController@index');*/
});

/*Route::get('/', function () {
    return view('admin.dashboard');
});

Route::get('/view_prod_cat', function () {
    return view('admin.product_category.product_category');
});

Route::get('/add_prod_cat', function () {
    return view('admin.product_category.add_product_category');
});

Route::get('/view_prod', function () {
    return view('admin.product.product');
});

Route::get('/add_prod', function () {
    return view('admin.product.add_product');
});*/

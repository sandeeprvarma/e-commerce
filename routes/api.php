<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a grou
Route::get('cart/products', 'API\CartController@index');
Route::get('cart/products', 'API\CartController@index');
Route::get('cart/products', 'API\CartController@index');p which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['assign.guard:api']],function() {
	Route::post('/register', 'AuthController@register');
	Route::post('/login', 'AuthController@authenticate');
	Route::post('/logout', 'AuthController@logout');
	Route::post('/verify', 'AuthController@verify');
	Route::post('/sendotp', 'AuthController@sendOTP');
	Route::get('/isUserVerified/{mob}', 'AuthController@isUserVerified');

	Route::get('products', 'API\ProductController@index');
	Route::get('product/{id}', 'API\ProductController@show');
	Route::get('product/category/{id}', 'API\ProductController@getProductsByCategory');
	Route::post('search', 'API\ProductController@search');

	Route::get('categories', 'API\CategoryController@index');
	Route::get('category/{id}', 'API\CategoryController@show');
	Route::get('category-pref/{id}', 'API\CategoryController@getCategoryByPref');

	Route::get('slider-data', 'API\UIController@index');

	Route::group(['middleware' => ['jwt.verify']], function() {
		Route::get('/user', 'AuthController@getAuthenticatedUser');

		Route::post('customer/address/add', 'API\CustomerAddressController@addAddress');
		Route::post('customer/address/{id}/edit', 'API\CustomerAddressController@editAddress');
		Route::get('customer/address/{id}/delete', 'API\CustomerAddressController@deleteAddress');
		Route::get('customer/address', 'API\CustomerAddressController@getAddresses');
		Route::get('customer/address/{address_type}', 'API\CustomerAddressController@getAddresses');

		Route::get('cart/products', 'API\CartController@index');
		Route::post('cart/add', 'API\CartController@store');
		Route::post('cart/update', 'API\CartController@store');
		Route::post('cart/remove', 'API\CartController@remove');
		Route::get('cart/remove/all', 'API\CartController@empty');
		Route::get('cart/summary', 'API\CartController@getCartSummary');

		Route::get('orders', 'API\OrderController@index');
		Route::post('order/create', 'API\OrderController@store');
	});
});